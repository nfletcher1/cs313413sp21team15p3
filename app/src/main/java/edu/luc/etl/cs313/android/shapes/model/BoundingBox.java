package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		final Shape shape = f.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		int x, y, width, height;
		int minX = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE,
				minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;

		if (g.getShapes().size() > 0) {
			for (Shape shape : g.getShapes()) {
				Location shapeBoundingBox = shape.accept(this);
				x = shapeBoundingBox.getX();
				y = shapeBoundingBox.getY();
				width = ((Rectangle)shapeBoundingBox.getShape()).getWidth();
				height = ((Rectangle)shapeBoundingBox.getShape()).getHeight();

				// maxX and maxY need to take width and height into account
				minX = Math.min(x, minX);
				maxX = Math.max(x + width, maxX);
				minY = Math.min(y, minY);
				maxY = Math.max(y + height, maxY);
			}

			width = maxX - minX;
			height = maxY - minY;

			return new Location(minX, minY, new Rectangle(width, height));
		}
		return null;
	}

	@Override
	public Location onLocation(final Location l) {
		final int x = l.getX();
		final int y = l.getY();
		final Location location = l.getShape().accept(this);
		// the location's shape's bounding box may have a different position than the location itself
		final int diffX = location.getX();
		final int diffY = location.getY();
		final Rectangle rectangle = (Rectangle)location.getShape();
		return new Location(x + diffX, y + diffY, rectangle);
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0, 0, new Rectangle(width, height));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		final Shape shape = c.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		final Shape shape = o.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		final int width, height;
		// initialize mins and maxes to max and min values
		int minX = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE,
				minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
		if (s.getPoints().size() > 0) {
			for (Point point : s.getPoints()) {
				int x = point.getX();
				int y = point.getY();

				minX = Math.min(x, minX);
				maxX = Math.max(x, maxX);
				minY = Math.min(y, minY);
				maxY = Math.max(y, maxY);
			}

			width = maxX - minX;
			height = maxY - minY;

			return new Location(minX, minY, new Rectangle(width, height));
		}
		return null;
	}
}
