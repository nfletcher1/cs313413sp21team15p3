package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		final int oldColor = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(oldColor);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		final Style oldStyle = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(oldStyle);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for (Shape shape : g.getShapes()) {
			shape.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		final int x = l.getX();
		final int y = l.getY();
		canvas.translate(x, y);
		l.getShape().accept(this);
		canvas.translate(-x, -y);
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		canvas.drawRect(0, 0, width, height, paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		final Style oldStyle = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(oldStyle);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		final Point[] points = (Point[])s.getPoints().toArray();
		float[] lilFloats = new float[points.length * 4];

		if (lilFloats.length > 0) {
			int n = 0;
			Point point = points[0];
			// first point goes in once
			lilFloats[n++] = point.getX();
			lilFloats[n++] = point.getY();
			for (int i = 1; i < points.length; i++) {
				point = points[i];
				// middle points go in twice
				lilFloats[n++] = point.getX();
				lilFloats[n++] = point.getY();
				lilFloats[n++] = point.getX();
				lilFloats[n++] = point.getY();
			}
			// first point goes in again to close the shape
			point = points[0];
			lilFloats[n++] = point.getX();
			lilFloats[n++] = point.getY();
		}

		canvas.drawLines(lilFloats, paint);
		return null;
	}
}
